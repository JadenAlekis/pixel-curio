﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System.Web;
using Microsoft.Extensions.Caching.Memory;

namespace pixel_curio.Controllers
{
    public class VueTubeController : Controller
    {
        private readonly IMemoryCache _memoryCache;
        private const string _cacheKey = "yt-videoid-cache-key";

        public VueTubeController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetRandomVideoAsync()
        {
            if (_memoryCache.TryGetValue(_cacheKey, out string cacheVideoId))
            {
                return Json(new { videoId = cacheVideoId, searchTerms = "Cache" });
            }

            //TODO remove this from source and regenerate key
            var youTubeService = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyC1LR8KuZjya7BonE2fISwKYWWJkojUy54" });

            Random rng = new Random((int)DateTime.Now.Ticks);

            string searchTerms = WordList.Words[rng.Next(0, WordList.Words.Length)] + " "
                                + WordList.Words[rng.Next(0, WordList.Words.Length)] + " "
                                + WordList.Words[rng.Next(0, WordList.Words.Length)];
            Debug.WriteLine("Using search terms - " + searchTerms);

            var searchListRequest = youTubeService.Search.List("snippet");
            searchListRequest.Q = searchTerms;
            searchListRequest.MaxResults = 30;

            var searchListResponse = await searchListRequest.ExecuteAsync();

            string foundVideoId = "dmVnsFSZSfw";

            bool breakout = false;
            foreach (var searchResult in searchListResponse.Items)
            {
                switch (searchResult.Id.Kind)
                {
                    case "youtube#video":
                        foundVideoId = searchResult.Id.VideoId;
                        breakout = true;
                        break;
                }

                if(breakout) break;
            }

            _memoryCache.Set(_cacheKey, foundVideoId, TimeSpan.FromSeconds(10));

            return Json(new { videoId = foundVideoId, searchTerms = searchTerms });
            }
    }
}