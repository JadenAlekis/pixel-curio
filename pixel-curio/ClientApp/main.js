﻿import Vue from 'vue'
import App from './Components/app.vue'

new Vue({
    el: '#app',
    render: h => h(App)
})

module.hot.accept();